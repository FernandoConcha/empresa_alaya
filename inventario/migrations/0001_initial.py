# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-10 17:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='inventario',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, unique=True)),
                ('nombre', models.CharField(blank=True, max_length=100)),
                ('Cantidad', models.IntegerField(max_length=100)),
                ('Pertenecia1', models.CharField(max_length=100)),
                ('Pertenecia2', models.CharField(max_length=100)),
            ],
        ),
    ]

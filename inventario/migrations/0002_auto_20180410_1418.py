# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-10 17:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inventario',
            name='Cantidad',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='inventario',
            name='nombre',
            field=models.CharField(max_length=100),
        ),
    ]

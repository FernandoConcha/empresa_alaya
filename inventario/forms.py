from django import forms
from .models import *
class RegForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    Cantidad = forms.IntegerField()
    Pertenecia1 = forms.CharField(max_length=100)
    Pertenecia2 = forms.CharField(max_length=100)
class RegModelForm(forms.ModelForm):
    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) >= 2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
    def clean_Cantidad(self):
        Cantidad = self.cleaned_data.get("Cantidad")
        if Cantidad:
            if Cantidad == 0:
                raise forms.ValidationError("No se puede ingresar valores en 0")
            else:
                return Cantidad



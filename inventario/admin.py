from django.contrib import admin

# Register your models here.
from .models import *
from .forms import *


class AdminInventario(admin.ModelAdmin):
        list_display = ["nombre","id","Cantidad"]
        form = RegModelForm
        list_filter = ["id"]
        #class Meta:
         #   model = inventario


admin.site.register(inventario,AdminInventario)
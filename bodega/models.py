from django.db import models

# Create your models here.
class bodega(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre = models.CharField(max_length=100, blank=True, null=False)
    cantidad = models.IntegerField(null=False)
    stock_min = models.PositiveIntegerField(null=False)
    ubicacion = models.CharField(max_length=100, null=False)
    fecha = models.DateTimeField(auto_now_add = True, auto_now= False)

    def _str_(self):
        return self.nombre
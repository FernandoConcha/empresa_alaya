from django.contrib import admin

# Register your models here.
from .models import *
from .forms import *


class Adminbodega(admin.ModelAdmin):
        list_display = ["nombre","cantidad","stock_min"]
        list_filter = ["id","fecha"]
        form = RegModelForm
        #class Meta:
         #   model = bodega


admin.site.register(bodega,Adminbodega)
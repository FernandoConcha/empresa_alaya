from django import forms
from .models import *
class RegModelForm(forms.ModelForm):
    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) >= 2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
    def clean_ubicacion(self):
        ubicacion = self.cleaned_data.get("ubicacion")
        if ubicacion:
            if len(ubicacion) >= 2 and len(ubicacion) <= 100:
                return ubicacion
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
    def clean_stock_min(self):
        stock_min = self.cleaned_data.get("stock_min")
        if stock_min:
            if stock_min == 0:
                raise forms.ValidationError("No se puede ingresar valores en 0")
            else:
                return stock_min



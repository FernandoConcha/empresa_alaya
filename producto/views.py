from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from .forms import RegForm
from .models import *
#def inicio(request):'
#    return render(request,"producto.html",{})

def inicioProducto(request):
    form = RegForm(request.POST or None)
    contexto = {
        "el_formulario":
            form,
    }
    if form.is_valid():
        form_data = form.cleaned_data

        nombre = form_data.get("nombre_producto")
        descripcion1 = form_data.get("descripcion")
        categoria1 = form_data.get("categoria")
        sub_categoria1 = form_data.get("sub_categoria")
        objeto = Producto.objects.create(nombre_producto=nombre, descripcion=descripcion1,categoria = categoria1, sub_categoria = sub_categoria1)


    return render(request, "producto.html", contexto)
from django import forms

class RegForm(forms.Form):

    nombre_producto =forms.CharField(max_length=100)
    descripcion = forms.CharField(max_length=200)
    categoria = forms.CharField(max_length=200)
    sub_categoria = forms.CharField(max_length=200)
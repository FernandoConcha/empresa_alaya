from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Producto(models.Model):
    id = models.AutoField(primary_key = True, unique = True)
    nombre_producto = models.CharField(max_length=100, blank=False, null = False)
    descripcion = models.CharField(max_length=200, blank=False, null=False)
    categoria = models.CharField(max_length=200, blank=False, null=False)
    sub_categoria = models.CharField(max_length=200, blank=False, null=False)


    def _str_(self):
        return self.nombre_producto
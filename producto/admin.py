from django.contrib import admin

# Register your models here.

from .models import *


class AdminProducto(admin.ModelAdmin):
        list_display = ["nombre_producto","descripcion"]
        list_filter = ["id"]
        class Meta:
            model = Producto


admin.site.register(Producto,AdminProducto)
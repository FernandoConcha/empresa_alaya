from django.contrib import admin

# Register your models here.
from .models import *



class Adminadquisiciones(admin.ModelAdmin):
        list_display = ["id","nombre_producto","nombre_proveedor","Cantidad"]
        list_filter = ["id","fecha"]
        class Meta:
            model = adquisiciones

admin.site.register(adquisiciones,Adminadquisiciones)
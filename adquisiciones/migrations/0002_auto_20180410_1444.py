# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-10 17:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adquisiciones', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='adquisiciones',
            name='numero_factura',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='adquisiciones',
            name='Cantidad',
            field=models.IntegerField(),
        ),
    ]

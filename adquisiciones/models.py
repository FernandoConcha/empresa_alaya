from django.db import models

# Create your models here.
class adquisiciones(models.Model):
    id = models.AutoField(primary_key = True, unique = True)
    nombre_producto = models.CharField(max_length=100, blank=False, null = False)
    nombre_proveedor = models.CharField(max_length=100, blank=False, null=False)
    Cantidad = models.IntegerField(null = False )
    fecha = models.DateField(null = False )
    numero_factura = models.IntegerField(blank = False, null = False, default= 0)

    def _str_(self):
        return self.nombre_producto